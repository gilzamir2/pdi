#include "intensityLevel.h"
#include "image.h"
#include <cmath>

void intensityLevel(const char *input, const char *output, int C1, int C2, int c1, int c2, int c3) {
    Image *img = readSimpleBMP(input);
    Image *img2 = createImage(img->width, img->height);

    for (int i = 0; i < img->width; i++) {
        for (int j = 0; j < img->height; j++) {
            pixel r = get_channel(img, i, j, RED);
            pixel g = get_channel(img, i, j, GREEN);
            pixel b = get_channel(img, i, j, BLUE);

            pixel rc, gc, bc;
            
            int A = C1; //valores entrados pelo usuário
            int B = C2; 

            //Nos 'if' seguintes eu ponho um OU LÓGICO para adaptar o código para imagens coloridas
            //Mas esse intensity-level slicing apresenta um melhor resultado para imagens em grayscale
            
            if((r >= 0 && r < A) || (g >= 0 && g < A) || (b>=0 && b<A)){
                rc = r;
                gc = g;
                bc = b;
            }
            if((r>= A && r <= B) || (g>=A && g<=B) || (b>=A && b<=B)){
                rc = c1; 
                gc = c2; 
                bc = c3; 
            }
            if((r > B && r <= GET_MASK[RED]) || (g>B && g<=GET_MASK[GREEN]) || (b>B && b<=GET_MASK[BLUE])){
                rc = r;
                gc = g;
                bc = b;
            }

            set_pixel(img2, i, j, rc, gc, bc);
        }
    }

    saveSimpleBMP(output, img2);
    deleteImage(img);
    deleteImage(img2);
}