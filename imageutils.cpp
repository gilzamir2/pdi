#include <QImage>
#include <QString>
#include "imageutils.h"

Image* loadImage(const char *path) {
    QString tgpath = path;
    QImage tgimg(tgpath);
    Image *img = new Image();
    img->width = tgimg.width();
    img->height = tgimg.height();
    img->bits = new pixel[img->width * img->height];
    for (int i = 0; i < img->width; i++) {
        for (int j = 0; j < img->height; j++) {
            QRgb c = tgimg.pixel(i,j);
            set_pixel(img, i, j, qRed(c), qGreen(c), qBlue(c));
        }
    }
    return img;
}

bool save(Image *img, const char *path){

    QImage qimg(img->width, img->height, QImage::Format_RGB32);

    for (int i = 0; i < img->width; i++) {
        for (int j = 0; j < img->height; j++) {
            pixel R = get_channel(img, i, j, RED);
            pixel G = get_channel(img, i, j, GREEN);
            pixel B = get_channel(img, i, j, BLUE);
            qimg.setPixel(i, j, qRgb(R, G, B));
        }
    }
    QString p = path;
    return qimg.save(p);
}
