#include "copyimg.h"
#include "image.h"

void copyimg(const char *src, const char *dest) {
    Image *srcImg = readSimpleBMP(src);
    Image *dstImg = createImage(srcImg->width, srcImg->height);

    for (size_t i = 0; i < srcImg->width; i++) {
        for (size_t j = 0; j < srcImg->height; j++) {
            pixel R = get_channel(srcImg, i, j, RED);
            pixel G = get_channel(srcImg, i, j, GREEN);
            pixel B = get_channel(srcImg, i, j, BLUE);

            set_pixel(dstImg, i, j, R, G, B);
        }
    }

    saveSimpleBMP(dest, dstImg);

    deleteImage(srcImg);
    deleteImage(dstImg);
}
