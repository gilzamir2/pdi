#ifndef IMAGEUTILS_H
#define IMAGEUTILS_H


#include "image.h"

Image* loadImage(const char *path);
bool save(Image *img, const char *path);

#endif // IMAGEUTILS_H
