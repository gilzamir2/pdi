#ifndef CONTRAST_H
#define CONTRAST_H

void contrast(const char *input, const char *output, size_t min, size_t max);
#endif // CONTRAST_H