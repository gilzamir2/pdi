#ifndef THRESHOLD_H
#define THRESHOLD_H

void threshold(const char *input, const char *output, size_t L);
#endif // THRESHOLD_H