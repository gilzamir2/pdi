#include "grayscale.h"
#include "image.h"
#include <cmath>

void grayscale(const char *input, const char *output) {
    Image *img = readSimpleBMP(input);
    Image *img2 = createImage(img->width, img->height);

    for (int i = 0; i < img->width; i++) {
        for (int j = 0; j < img->height; j++) {
            pixel r = get_channel(img, i, j, RED);
            pixel g = get_channel(img, i, j, GREEN);
            pixel b = get_channel(img, i, j, BLUE);

            pixel l = (pixel)round(r * 0.3f + g * 0.59 + b * 0.11);
            set_pixel(img2, i, j, l, l, l);
        }
    }

    saveSimpleBMP(output, img2);
    deleteImage(img);
    deleteImage(img2);
}
