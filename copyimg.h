#ifndef COPYIMG_H
#define COPYIMG_H

//Copia uma imagem do path src para o path dest.
//Apenas aceita imagem bmp
void copyimg(const char *src, const char *dest);

#endif // COPYIMG_H
