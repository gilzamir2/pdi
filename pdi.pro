#-------------------------------------------------
#
# Project created by QtCreator 2015-04-06T16:59:50
#
#-------------------------------------------------

QT       += core

QT       += gui

TARGET = histogram2
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp \
    histograma.cpp \
    grayscale.cpp \
    image.cpp \
    imageutils.cpp \
    copyimg.cpp

HEADERS += \
    image.h \
    imageutils.h \
    histograma.h \
    grayscale.h \
    copyimg.h
