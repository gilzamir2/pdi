#include "negative.h"
#include "image.h"
#include <cmath>

void negative(const char *input, const char *output) {
    Image *img = readSimpleBMP(input);
    Image *img2 = createImage(img->width, img->height);

    for (int i = 0; i < img->width; i++) {
        for (int j = 0; j < img->height; j++) {
            pixel r = get_channel(img, i, j, RED);
            pixel g = get_channel(img, i, j, GREEN);
            pixel b = get_channel(img, i, j, BLUE);

            pixel rc = GET_MASK[RED] - r;
            pixel gc = GET_MASK[GREEN] - g;
            pixel bc = GET_MASK[BLUE] - b;

            set_pixel(img2, i, j, rc, gc, bc);
        }
    }

    saveSimpleBMP(output, img2);
    deleteImage(img);
    deleteImage(img2);
}