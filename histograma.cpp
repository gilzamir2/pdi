#include "histograma.h"
#include "image.h"
#include <iostream>
#include <cmath>

using namespace std;

//FUNCAO AUXILIAR, NAO PRECISA DECLARAR NO HEADER (histograma.h)
void minmax(size_t v[], size_t n, size_t &min, size_t &max) {
    size_t lmax = 0;
    size_t lmin = 0;
    for (size_t i = 0; i < n; i++) {
        if (v[i] >= v[lmax]) {
            lmax = i;
        } else if (v[i] <= v[lmin]) {
            lmin = i;
        }
    }
    min = v[lmin];
    max = v[lmax];
    cout<<min<<", "<<max<<endl;
}



//IMPLEMENTACAO DA FUNCAO HISOGRAMA DECLARADA NO HEADER
void histograma(const char *inputpath, const char *outputpath) {

    Image *img = readSimpleBMP(inputpath);
    if (img) {
        size_t cont[256];
        pixel r;
        size_t i, j;
        for (i = 0; i < 256; i++) {
            cont[i] = 0;
        }

        for (i = 0; i < img->width; i++) {
            for (j = 0; j < img->height; j++) {
                r = get_channel(img, i, j, RED); //pega a cor do pixel das coordenadas, como imagem é P&B so basta um canal
                cont[r] = cont[r] + 1;
            }
        }

        size_t max, min;
        minmax(cont, 256, min, max);

        Image *img2 = createImage(512, 250);

        for(i = 0; i < img2->width; i++){
            for(j = 0; j < img2->height; j++){
                set_pixel(img2, i, j, 255, 255, 255); //preenche a imagem 2 com branco
            }
        }

        size_t horizontalSize = img2->width/256;

        for(i = 0; i < 256; i++){
            size_t j = img2->height-1;

            float f = ((float)(cont[i]))/(max);
            size_t n = (size_t)(f * img2->height);
            cout<<f<<endl;
            while(n > 0){
                for (size_t h = 0; h < horizontalSize; h++) {
                    set_pixel(img2, i * horizontalSize + h, j, 0, 0, 255); //Escolhi preencher o histograma em azul para dar mais destaque
                }
                n--;
                j--;
            }
        }


        if (saveSimpleBMP(outputpath, img2)) {
            cout<<"OK!"<<endl;
        } else {
            cout<<"ERROR"<<endl;
        }

        //libera espaco utilizado pela imagem
        deleteImage(img);
        deleteImage(img2);
    }
}
