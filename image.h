#ifndef IMAGE_H
#define IMAGE_H

#define RED 0
#define GREEN 1
#define BLUE 2
#define NUMBER_OF_CHANNELS 4
#define CHANNEL_SIZE 8

#include <cstdlib>

using namespace std;

typedef unsigned int pixel;

typedef struct ImageType {
    pixel *bits;
    size_t width;
    size_t height;
} Image;

Image* createImage(size_t width, size_t height);
void deleteImage(Image *img);
int get_channel(Image *img, int l, int c, size_t channel);
void set_pixel(Image *image, int l, int c, pixel R, pixel G, pixel B);
Image* readSimpleBMP(const char *filename);
bool saveSimpleBMP(const char *path, Image *source);

#endif // IMAGE_H
