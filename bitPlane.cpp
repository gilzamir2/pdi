#include "bitPlane.h"
#include "image.h"
#include <cmath>

void bitPlane(const char *input, const char *output){

    Image *img = readSimpleBMP(input);
    Image *img0 = createImage(img->width,img->height);
    Image *img1 = createImage(img->width,img->height);
    Image *img2 = createImage(img->width,img->height);
    Image *img3 = createImage(img->width,img->height);
    Image *img4 = createImage(img->width,img->height);
    Image *img5 = createImage(img->width,img->height);
    Image *img6 = createImage(img->width,img->height);
    Image *img7 = createImage(img->width,img->height);
    Image *img8 = createImage(img->width,img->height); //Imagem apenas para comparar com as outras

    pixel r, g, b, c;
    size_t i, j;

    for (i = 0; i < img->width; i++) {
        for (j = 0; j < img->height; j++) {
            r = get_channel(img, i, j, RED);
            g = get_channel(img, i, j, GREEN);
            b = get_channel(img, i, j, BLUE);

            c = round(0.3*r + 0.59*g + 0.11*b);
            //cout<<c<<endl;
            
            //A imagem de entrada em tons de cinza, usada apenas para comparar com o resultado das outras imagens
            set_pixel(img8, i, j, c, c, c);

            if(c >= 128){
                set_pixel(img7,i,j,c,c,c);
                set_pixel(img6,i,j,0,0,0);
                set_pixel(img5,i,j,0,0,0);
                set_pixel(img4,i,j,0,0,0);
                set_pixel(img3,i,j,0,0,0);
                set_pixel(img2,i,j,0,0,0);
                set_pixel(img1,i,j,0,0,0);
                set_pixel(img0,i,j,0,0,0);
            }
            else{
                if(c >= 64){
                    set_pixel(img6,i,j,c,c,c);
                    set_pixel(img7,i,j,0,0,0);
                    set_pixel(img5,i,j,0,0,0);
                    set_pixel(img4,i,j,0,0,0);
                    set_pixel(img3,i,j,0,0,0);
                    set_pixel(img2,i,j,0,0,0);
                    set_pixel(img1,i,j,0,0,0);
                    set_pixel(img0,i,j,0,0,0);
                }else{
                    if(c >= 32){
                        set_pixel(img5,i,j,c,c,c);
                        set_pixel(img6,i,j,0,0,0);
                        set_pixel(img7,i,j,0,0,0);
                        set_pixel(img4,i,j,0,0,0);
                        set_pixel(img3,i,j,0,0,0);
                        set_pixel(img2,i,j,0,0,0);
                        set_pixel(img1,i,j,0,0,0);
                        set_pixel(img0,i,j,0,0,0);
                    }else{
                        if(c >= 16){
                            set_pixel(img4,i,j,c,c,c);
                            set_pixel(img6,i,j,0,0,0);
                            set_pixel(img5,i,j,0,0,0);
                            set_pixel(img7,i,j,0,0,0);
                            set_pixel(img3,i,j,0,0,0);
                            set_pixel(img2,i,j,0,0,0);
                            set_pixel(img1,i,j,0,0,0);
                            set_pixel(img0,i,j,0,0,0);
                        }else{
                            if(c >= 8){
                                set_pixel(img3,i,j,c,c,c);
                                set_pixel(img6,i,j,0,0,0);
                                set_pixel(img5,i,j,0,0,0);
                                set_pixel(img7,i,j,0,0,0);
                                set_pixel(img4,i,j,0,0,0);
                                set_pixel(img2,i,j,0,0,0);
                                set_pixel(img1,i,j,0,0,0);
                                set_pixel(img0,i,j,0,0,0);
                            }else{
                                if(c >= 4){
                                    set_pixel(img2,i,j,c,c,c);
                                    set_pixel(img6,i,j,0,0,0);
                                    set_pixel(img5,i,j,0,0,0);
                                    set_pixel(img7,i,j,0,0,0);
                                    set_pixel(img3,i,j,0,0,0);
                                    set_pixel(img4,i,j,0,0,0);
                                    set_pixel(img1,i,j,0,0,0);
                                    set_pixel(img0,i,j,0,0,0);
                                }else{
                                    if(c >= 2){
                                        set_pixel(img1,i,j,c,c,c);
                                        set_pixel(img6,i,j,0,0,0);
                                        set_pixel(img5,i,j,0,0,0);
                                        set_pixel(img7,i,j,0,0,0);
                                        set_pixel(img3,i,j,0,0,0);
                                        set_pixel(img2,i,j,0,0,0);
                                        set_pixel(img4,i,j,0,0,0);
                                        set_pixel(img0,i,j,0,0,0);
                                    }else{
                                        set_pixel(img0,i,j,c,c,c);
                                        set_pixel(img6,i,j,0,0,0);
                                        set_pixel(img5,i,j,0,0,0);
                                        set_pixel(img7,i,j,0,0,0);
                                        set_pixel(img3,i,j,0,0,0);
                                        set_pixel(img2,i,j,0,0,0);
                                        set_pixel(img4,i,j,0,0,0);
                                        set_pixel(img1,i,j,0,0,0);
                                    }
                                }
                            }
                        }
                    }
                }
            }

       }
    }

    saveSimpleBMP(output, img0);
    saveSimpleBMP(output, img1);
    saveSimpleBMP(output, img2);
    saveSimpleBMP(output, img3);
    saveSimpleBMP(output, img4);
    saveSimpleBMP(output, img5);
    saveSimpleBMP(output, img6);
    saveSimpleBMP(output, img7);

    deleteImage(img);
    deleteImage(img0);
    deleteImage(img1);
    deleteImage(img2);
    deleteImage(img3);
    deleteImage(img4);
    deleteImage(img5);
    deleteImage(img6);
    deleteImage(img7);

}