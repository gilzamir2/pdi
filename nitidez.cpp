#include "nitidez.h"
#include "image.h"
#include <cmath>

void nitidez(const char *input, const char *output){

    Image *img = loadImage(input);
    Image *img2 = createImage(img->width, img->height);
    Image *img3 = createImage(img->width, img->height);

    pixel r1, r2, r3, r4, rA, r5, r6, r7, r8, medR, medG, medB, opR, opG, opB;
    size_t i, j;

    for (i = 0; i < img->width; i++) {
        for (j = 0; j < img->height; j++) {
            if((i!=0) && (i!=img->width) && (j!=0) && (j!=img->height)){
                  r1 = get_channel(img, i-1, j-1, RED);
                   r2 = get_channel(img, i-1, j  , RED);
                   r3 = get_channel(img, i-1, j+1, RED);
                   r4 = get_channel(img, i  , j-1, RED);
                   rA = get_channel(img, i  , j  , RED);  //pixel que vai ser aplicado o filtro
                   r5 = get_channel(img, i  , j+1, RED);
                   r6 = get_channel(img, i+1, j-1, RED);
                   r7 = get_channel(img, i+1, j  , RED);
                   r8 = get_channel(img, i+1, j+1, RED);
                   medR = (pixel) (r1 + r2 + r3 + r4 + rA + r5 + r6 + r7 + r8)/9;

                   r1 = get_channel(img, i-1, j-1, GREEN);
                   r2 = get_channel(img, i-1, j  , GREEN);
                   r3 = get_channel(img, i-1, j+1, GREEN);
                   r4 = get_channel(img, i  , j-1, GREEN);
                   rA = get_channel(img, i  , j  , GREEN);  //pixel que vai ser aplicado o filtro
                   r5 = get_channel(img, i  , j+1, GREEN);
                   r6 = get_channel(img, i+1, j-1, GREEN);
                   r7 = get_channel(img, i+1, j  , GREEN);
                   r8 = get_channel(img, i+1, j+1, GREEN);
                   medG = (pixel) (r1 + r2 + r3 + r4 + rA + r5 + r6 + r7 + r8)/9;

                   r1 = get_channel(img, i-1, j-1, BLUE);
                   r2 = get_channel(img, i-1, j  , BLUE);
                   r3 = get_channel(img, i-1, j+1, BLUE);
                   r4 = get_channel(img, i  , j-1, BLUE);
                   rA = get_channel(img, i  , j  , BLUE);  //pixel que vai ser aplicado o filtro
                   r5 = get_channel(img, i  , j+1, BLUE);
                   r6 = get_channel(img, i+1, j-1, BLUE);
                   r7 = get_channel(img, i+1, j  , BLUE);
                   r8 = get_channel(img, i+1, j+1, BLUE);
                   medB = (pixel) (r1 + r2 + r3 + r4 + rA + r5 + r6 + r7 + r8)/9;

                   set_pixel(img2, i, j, medR , medG, medB);

                   pixel R1 = get_channel(img, i, j, RED);
                   pixel R2 = get_channel(img2, i, j, RED);
                   pixel G1 = get_channel(img, i, j, GREEN);
                   pixel G2 = get_channel(img2, i, j, GREEN);
                   pixel B1 = get_channel(img, i, j, BLUE);
                   pixel B2 = get_channel(img2, i, j, BLUE);

                   if(R2 < R1)
                         Rx = R1 - R2;
                   else
                         Rx = 0;
                   if(G2 < G1)
                         Gx = G1 - G2;
                   else
                         Gx = 0;
                   if(B2 < B1)
                         Bx = B1 - B2;
                   else
                         Bx = 0;

                   Rx = R1 + 3*Rx;
                   Gx = G1 + 3*Gx;
                   Bx = B1 + 3*Bx;

                    if(Rx > 255){
                         Rx = 255;
                    }
                    if(Gx > 255){
                         Gx = 255;
                    }
                    if(Bx > 255){
                         Bx = 255;
                    }

                    set_pixel(img3, i, j, Rx, Gx, Bx);
           }
       }
    }

    saveSimpleBMP(output, img3);
    
    deleteImage(img);
    deleteImage(img2);
    deleteImage(img3);

}