#include "contrast.h"
#include "image.h"
#include <cmath>

void contrast(const char *input, const char *output, size_t min, size_t max){

    Image *img = readSimpleBMP(input);
    Image *img2 = createImage(img->width, img->height);

    pixel r, rc;
    size_t i, j;
    
    for(i = 0; i < img->width; i++){
        for(j = 0; j< img->height; j++){
            r = get_channel(img, i, j, RED);

            if((r >= 0) && (r < min)){
                rc = 0;
            }
            if((r >= min) && (r < max)){
                rc = ((r - min)*GET_MASK[RED])/(max - min);
            }
            if((r >= max) && (r <= GET_MASK[RED])){
                rc = GET_MASK[RED];
            }

            set_pixel(img2, i, j, rc, rc, rc);
        }
    }

    saveSimpleBMP(output, img2);
    deleteImage(img);
    deleteImage(img2);
}