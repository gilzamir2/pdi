#-------------------------------------------------
#
# Project created by QtCreator 2015-04-06T16:59:50
#
#-------------------------------------------------

QT       += core

QT       += gui

TARGET = histogram2
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp

HEADERS += \
    image.h
