#include "threshold.h"
#include "image.h"
#include <cmath>

void threshold(const char *input, const char *output, size_t L){

    //L eh o limiar entrado pelo usuario
    Image *img = readSimpleBMP(input);
    Image *img2 = createImage(img->width, img->height);

    for(size_t i = 0; i< img->width; i++){
        for(size_t j = 0; j< img->height; j++){
            pixel r = get_channel(img, i, j, RED);
            pixel g = get_channel(img, i, j, GREEN);
            pixel b = get_channel(img, i, j, BLUE);

            pixel l = (pixel)round(r * 0.3f + g * 0.59 + b * 0.11);

            if(l >= L){
                l = 255;
            }else{
                l = 0;
            }
            set_pixel(img2, i, j, l, l, l);
        }
    }

    saveSimpleBMP(output, img2);
    deleteImage(img);
    deleteImage(img2);
}